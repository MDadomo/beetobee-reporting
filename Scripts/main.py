import Fonct.connect_sql as cosql
import Fonct.fonc_Excel as excel
import Fonct.fonc_sql as sql
import Fonct.dataFrame as df

# Liens  Excel
link = '../Data/Excel/Suivis BeeToBee.xlsx'


##################
# Traitement Excel

# Importation excel Dataframe
dataF = excel.readExcel(link)
print(dataF.head())

# Get dernière date de modif
dateModif = excel.dateModif(link)


##################
# Traitement DataFrame

# Vérification des données
dataFrameValid = df.verifData(dataF)



##################
# Traitement SQL

# Connection DataBase
connection = sql.connect()
db_cursor = connection.cursor()

# Get nom colonne
colname = df.colname(dataF)
print(colname)

# Creat Table
db_cursor.execute("CREATE TABLE IF NOT EXISTS student (id INT, name VARCHAR(255))")
db_cursor.execute("SHOW TABLES")
for table in db_cursor:
	print(table)
