import pandas as pd
import os.path, time

# Importation excel Dataframe
def readExcel(link):
    readExcel = pd.read_excel(link, index_col=0)
    return readExcel

# GET dernière date de modification du fichier
def dateModif(link):
    dateModif = time.ctime(os.path.getctime(link))
    print("Dernière date de modification : %s" % time.ctime(os.path.getctime(link)))
    return dateModif

