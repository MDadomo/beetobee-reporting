# coding: utf-8
import mysql.connector


def connect():
    db_connection = mysql.connector.connect(
        user='root',
        password='root',
        port='8889',
        host='127.0.0.1',
        database='BeeToBee',
        raise_on_warnings=True,
    )
    print('db_connection')
    print(db_connection)
    return db_connection
